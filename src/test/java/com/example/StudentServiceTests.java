package com.example;

import com.example.model.StudentModel;
import com.example.service.StudentService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by harunakaze on 06-May-17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class StudentServiceTests {
    @Autowired
    StudentService service;

    @Test
    public void testSelectAllStudents()
    {
        List<StudentModel> students = service.selectAllStudents();

        Assert.assertNotNull("Gagal = student menghasilkan null", students);
        Assert.assertEquals("Gagal - size students tidak sesuai",3, students.size());

    }

    @Test
    public void testSelectStudent() {
        StudentModel student = service.selectStudent("123");
        Assert.assertNotNull("Gagal - student menghasilkan nilai null", student);
        Assert.assertEquals("Gagal - nama mahasiswa tidak sesuai dengan database", "Ujang", student.getName());
        Assert.assertEquals("Gagal - GPA tidak sesuai dengan database", 2.0, student.getGpa(), 0.0);
    }

    @Test
    public void testCreateStudent()
    {
        StudentModel student = new StudentModel("126", "Budi", 3.44, null);

        // Cek apakah student sudah ada
        Assert.assertNull("Mahasiswa sudah ada", service.selectStudent(student.getNpm()));

        // Masukkan ke service
        service.addStudent(student);

        // Cek apakah student berhasil dimasukkan
        Assert.assertNotNull("Mahasiswa gagal dimasukkan", service.selectStudent(student.getNpm()));
    }

    @Test
    public void testUpdateStudent() {
        StudentModel oldStudent = service.selectStudent("123");

        // Updating model data
        oldStudent.setGpa(4.0);
        oldStudent.setName("Tomyhawk");

        // Updating
        service.updateStudent(oldStudent);

        StudentModel newStudent = service.selectStudent("123");

        // Data checking
        Assert.assertNotNull("Gagal - student menghasilkan nilai null", newStudent);
        Assert.assertEquals("Gagal - nama mahasiswa tidak sesuai dengan database", "Tomyhawk", newStudent.getName());
        Assert.assertEquals("Gagal - GPA tidak sesuai dengan database", 4.0, newStudent.getGpa(), 0.0);
    }

    @Test
    public void testDeleteStudent() {
        StudentModel student = service.selectStudent("123");

        Assert.assertNotNull("Gagal - student menghasilkan nilai null", student);

        // Delete student
        service.deleteStudent(student.getNpm());

        StudentModel deletedStudent = service.selectStudent("123");

        // Check is null
        Assert.assertNull("Mahasiswa gagal dihapus", deletedStudent);
    }
}
