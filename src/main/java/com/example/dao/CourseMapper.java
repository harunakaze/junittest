package com.example.dao;

import com.example.model.CourseModel;
import com.example.model.StudentModel;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CourseMapper {
    @Select("select id_course, nama, sks from course where id_course = #{idCourse}")
    @Results(value = {
            @Result(property="idCourse", column="id_course"),
            @Result(property="nama", column="nama"),
            @Result(property="sks", column="sks"),
            @Result(property="students", column="id_course",
                    javaType = List.class,
                    many=@Many(select="selectStudents"))
    })
    CourseModel selectCourse (@Param("idCourse") String idCourse);

    @Select("select id_course, nama, sks from course")
    @Results(value = {
            @Result(property="idCourse", column="id_course"),
            @Result(property="nama", column="nama"),
            @Result(property="sks", column="sks"),
            @Result(property="students", column="id_course",
                    javaType = List.class,
                    many=@Many(select="selectStudents"))
    })
    List<CourseModel> selectAllCourses ();

    @Insert("INSERT INTO course (id_course, nama, sks) VALUES (#{idCourse}, #{nama}, #{sks})")
    void addCourse (CourseModel course);

    @Delete("DELETE FROM course WHERE id_course = #{idCourse}")
    void deleteCourse (@Param("idCourse") String idCourse);

    @Update("UPDATE course SET nama = #{nama}, sks = #{sks} WHERE id_course = #{idCourse}")
    void updateCourse(CourseModel course);

    @Select("select student.npm, name, gpa " +
            "from student join studentcourse " +
            "on studentcourse.npm = student.npm " +
            "where studentcourse.id_course = #{idCourse}")
    List<StudentModel> selectStudents (@Param("idCourse") String idCourse);
}
